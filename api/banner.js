export default axios => ({
  list(params = {}) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "banner", {params: params});
    return response;
  },

  create(data) {
    let formData = new FormData();
    formData.append('file', data.media, data.media.name);
    formData.append('data', JSON.stringify(data));
    const response = axios.post(process.env.VUE_APP_API_BASE_URL + "banner", formData);
    return response;
  },

  edit(id, data) {
    console.log(data);
    if (data.media) {
      let formData = new FormData();
      formData.append('file', data.media, data.media.name);
      formData.append('data', JSON.stringify(data));
      const response = axios.patch(process.env.VUE_APP_API_BASE_URL + "banner/" + id, formData);
      return response;
    }else{
      const response = axios.patch(process.env.VUE_APP_API_BASE_URL + "banner/" + id, data);
      return response;
    }
  },

  view(id) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "banner/" + id);
    return response;
  },

  delete(id) {
    const response = axios.delete(process.env.VUE_APP_API_BASE_URL + "banner/" + id);
    return response;
  },

  getOptions() {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "banner/options");
    return response;
  }

});
