export default axios => ({
  list(params = {}) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "location", {params: params});
    return response;
  },

  listTrashed(params = {}) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "location/trashed", {params: params});
    return response;
  },

  create(data) {
    const response = axios.post(process.env.VUE_APP_API_BASE_URL + "location", data);
    return response;
  },

  edit(id, data) {
    const response = axios.patch(process.env.VUE_APP_API_BASE_URL + "location/" + id, data);
    return response;
  },

  view(id) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "location/" + id);
    return response;
  },

  delete(id) {
    const response = axios.delete(process.env.VUE_APP_API_BASE_URL + "location/" + id);
    return response;
  },

  restore(id) {
    const response = axios.put(process.env.VUE_APP_API_BASE_URL + "location/restore/" + id);
    return response;
  },

});
