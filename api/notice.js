import fileDownload from "js-file-download";

export default axios => ({
  list(params = {}) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "notice", {params: params});
    return response;
  },

  view(id) {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "notice/" + id);
    return response;
  },

  fetchDashboardData(params = {}) {
    const response = axios.post(process.env.VUE_APP_API_BASE_URL + "notice/findNoticeRecordByDate", params);
    return response;
  },

  exportData(params = {}) {
    axios.post(process.env.VUE_APP_API_BASE_URL + "notice/exportData", {
      params: params,
    })
      .then((response) => {
        fileDownload(response.data, response.headers['x-file-name']);
      })
      .catch(this.handleApiErrors)
      .finally(() => {

      });
  },

  getOptions() {
    const response = axios.get(process.env.VUE_APP_API_BASE_URL + "notice/options");
    return response;
  }

});
