import Notice from "@/api/notice";
import Location from "@/api/location";
import Banner from "@/api/banner";

export default (context, inject) => {

  // Initialize API factories
  const factories = {
    notice: Notice(context.$axios),
    location: Location(context.$axios),
    banner: Banner(context.$axios)
  };

  // Inject $api
  inject("api", factories);
};
