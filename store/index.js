export const state = () => ({
    messages: [],
})

export const mutations = {
    addMessage(state, payload) {
        payload.active = true;
        state.messages.push(payload);
    },
};
